use nom::{
    bytes::complete::{is_not, tag},
    character::complete::one_of,
    error::ErrorKind,
    multi::{many0, separated_list},
    sequence::delimited,
    IResult,
};

#[derive(Debug, PartialEq)]
pub struct Element<'a> {
    pub name: &'a str,
    pub attributes: Vec<&'a str>,
    pub children: Vec<Element<'a>>,
}

// /// Parses an inputted `sxml` string into an easy-to-use Result-like enum,
// /// provided by the `nom` library
// /// [here](https://docs.rs/nom/5.0.1/nom/type.IResult.html).
// /// 
// /// # Result handling
// /// 
// /// If this function succeeds, it will return a tuple of any remaining input
// /// and the [crate::Element] enum, containing all data for the parsed file. If
// /// this function fails, it will return a standardized `nom` error that will
// /// have a packed tuple with the stopped input and the method that caused the
// /// failure.
// /// 
// /// # Examples
// /// 
// /// ```
// /// use sxml::{Element, parse_sxml};
// ///
// /// fn main() {
// ///     let example_data = "<hi attribute='first element'><two></two></hi><other></other>";
// ///     let expected_result = vec![
// ///         Element {
// ///             name: "hi",
// ///             attributes: vec!["first element"],
// ///             children: vec![
// ///                 Element {
// ///                     name: "two",
// ///                     attributes: vec![],
// ///                     children: vec![]
// ///                 }
// ///             ]
// ///         },
// ///         Element {
// ///             name: "other",
// ///             attributes: vec![],
// ///             children: vec![]
// ///         }
// ///     ];
// ///
// ///     assert_eq!(parse_sxml(example_data), Ok(("", expected_result)));
// /// }
// /// ```
// pub fn parse_sxml(input: &mut str) -> IResult<&str, Vec<Element>> {
//     let mut base_elements: Vec<Element> = Vec::new();
    
//     let mut eof = false;

//     while !eof {
//         let (remaining, new_element) = element_parser(input)?;

//         base_elements.push(new_element);

//         if remaining == "" {
//             eof = true;
//         }
//         else {
//             input = remaining.clone();
//         }
//     }

//     Ok((
//         input,
//         Element {
//             name: "body",
//             attributes: vec![],
//             children: base_elements,
//         },
//     ))
// }

/// Gets an element and recurses to parse it's children. This parser returns
/// the [crate::Element] structure along with any remaining input.
fn element_parser(input: &str) -> IResult<&str, Element> {
    let (input, (name, attributes)) = element_start_tag(input)?;
    let (input, children) = many0(element_parser)(input)?;
    let (input, found_name) = delimited(tag("</"), is_not(">"), tag(">"))(input)?;

    if found_name == name {
        Ok((
            input,
            Element {
                name,
                attributes,
                children,
            },
        ))
    } else {
        Err(nom::Err::Error((input, ErrorKind::Tag)))
    }
}

/// Gives the starting tag of an element and returns
/// `([input], ([element name], [element arguments]))` inside of a nom
/// `IResult<&str, (&str, Vec<&str>)>`.
fn element_start_tag(input: &str) -> IResult<&str, (&str, Vec<&str>)> {
    let (input, name) = element_name_parser(input)?;
    let (input, attributes) = attribute_multi_parser(input)?;
    let (input, _) = tag(">")(input)?;

    Ok((input, (name, attributes)))
}

/// Gets `hello` from `<hello [etc]`. Suppost to be a hacky
/// way of properly getting a str name.
fn element_name_parser(input: &str) -> IResult<&str, &str> {
    delimited(tag("<"), is_not(" "), tag(" "))(input)
}

/// Assembles many `attribute="hi" attribute="x"` into `Vec<"hi", "x">`.
fn attribute_multi_parser(input: &str) -> IResult<&str, Vec<&str>> {
    separated_list(tag(" "), attribute_parser)(input)
}

/// Gets `hi` from `attribute="hi"` or `attribute='hi'`.
fn attribute_parser(input: &str) -> IResult<&str, &str> {
    let (input, _) = tag("attribute=")(input)?;
    string_quote_parser(input)
}

/// Gets `hi` from `"hi"`.
fn string_quote_parser(input: &str) -> IResult<&str, &str> {
    let allowed_quote_characters = r#""'"#;

    delimited(
        one_of(allowed_quote_characters),
        is_not(allowed_quote_characters),
        one_of(allowed_quote_characters),
    )(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_element() {
        let expected_result = Element {
            name: "hello",
            attributes: vec!["woooot", "hi"],
            children: vec![],
        };

        assert_eq!(
            element_parser("<hello attribute='woooot' attribute='hi'></hello>"),
            Ok(("", expected_result))
        );
    }
}
